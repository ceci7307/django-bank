from django.core.mail import send_mail


def email_message(message_dict):
   contents = f"""
   Hi, thank you for trying to reset your password.
   Your token is: {message_dict['reset_link']}
   """
   send_mail(
      'Password Reset Link',
      contents,
      'emil593g@stud.kea.dk',
      message_dict['email_reciever'],
      fail_silently=False
   )