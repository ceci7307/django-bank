from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    account_name = models.CharField(max_length=250)
  

    @classmethod
    def create(cls, user, account_name, balance):
        account = cls()
        account.user = user
        account.account_name = account_name
        account.save()
    
    def __str__(self):
        return f"{self.account_name}"

class UserProfile (models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.IntegerField(default = 0)
    membership = models.CharField(max_length = 50, default = None)
    role = models.CharField(max_length = 50, default = None)

    @classmethod
    def create(cls, user):
        userprofile = cls()
        userprofile.user = user
        userprofile.save() 

    def __str__(self):
        return f"{self.user}"

class Transaction (models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.IntegerField(default = 0)
    transaction_id = models.CharField(max_length = 50, default = None)
    description = models.CharField(max_length = 250, default = None)
    account_to = models.CharField(max_length = 50, default = None)

    @classmethod
    def create(cls, account):
        transaction = cls()
        transaction.account = account
        transaction.save() 

    def __str__(self):
        return f"{self.account}"


def loan(request):
    return render(request, "bank_app/loan.html")
