from django.contrib import admin
from django.urls import path
from . import views

app_name = 'bank_app'

urlpatterns = [
    path('', views.index, name='index'),
    path('bank/', views.bank, name='bank'),
    path('sign_up/', views.sign_up, name='sign_up'),
    path('<int:pk>/',views.single_customer,name='single_customer'),
    path('transaction/',views.transaction,name='transaction'),
    path('loan/',views.loan,name='loan'),
    path('basic_member/',views.basic_member,name='basic_member'),
    path('update_status/', views.update_status, name='update_status'),
]

