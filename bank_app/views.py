from django.shortcuts import render, get_object_or_404, reverse
from django.contrib.auth.models import User
from .models import Account
from django.http import HttpResponseRedirect
from . import models
from .models import UserProfile
from .models import Transaction
import uuid 

from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    if request.method == 'POST':
        Account.create(request.user, request.POST['text'])
  
     
         
    accounts = Account.objects.all().filter(user=request.user)

    context = {
        'accounts': accounts,
        

    }
    return render (request, 'bank_app/index.html', context)

@login_required
def transaction(request):
    my_accounts = Account.objects.all().filter(user=request.user)
    accounts = Account.objects.all()
    ran_number = uuid.uuid4().hex[:6].upper()
    account_name = 2
    transactions = Transaction.objects.all().filter(account=account_name)
  
    
    
    if request.method == 'POST':
        transaction = Transaction()
        account_from = Account.objects.get(account_name=request.POST["from_account"])
        transaction.account = account_from
        transaction.amount = request.POST["amount"]
        transaction.description = request.POST["description"]
        transaction.account_to = request.POST["to_account"]
        transaction.transaction_id = request.POST["transaction_id"]
        transaction.save()
        if transaction:
            transaction = Transaction()
            account_to = Account.objects.get(account_name=request.POST["to_account"])
            transaction.account = account_to
            transaction.amount = request.POST["amount"]
            transaction.description = request.POST["description"]
            transaction.account_to = request.POST["from_account"]
            transaction.transaction_id = request.POST["transaction_id"]
            transaction.save()
    context = {
        'my_accounts': my_accounts,
        'accounts': accounts,
        'ran_number': ran_number,
        'transactions': transactions
    }


    return render (request, 'bank_app/transaction.html', context)






@login_required
def loan(request):
    if request.method == 'POST':
        Account.create(request.user, "Loan")
    return render(request, "bank_app/loan.html")

@login_required
def basic_member(request):
    if request.method == 'POST':
        Account.create(request.user, request.POST['text'])

    accounts = Account.objects.all().filter(user=request.user)
    context = {
        'accounts': accounts,
    }
    return render (request, 'bank_app/basic_member.html', context)

@login_required
def bank(request):
    if request.method == "POST":
        text = request.POST["text"]
        user = User()
        user.username = username
        user.save()

    users = User.objects.all()
    context = {
        'users' : users,
    }
    return render (request, 'bank_app/bank.html', context)

@login_required
def sign_up(request):
    context = {}
    if request.method == 'POST':
        username = request.POST['username']
        first_name = request.POST['firstname']
        last_name = request.POST['lastname']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        email = request.POST['email']

        phone = request.POST['phone']
        role = request.POST['role']
        membership = request.POST['membership']

        if password == confirm_password:
            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            if user:
                userprofile = UserProfile()
                userprofile.user = user
                userprofile.phone = phone
                userprofile.role = role
                userprofile.membership = membership
                userprofile.save()
                return HttpResponseRedirect(reverse('bank_app:bank'))
            else:
                context = {'error': 'Could not create user - try something else'}
        else:
            context ={'error': 'Passwords do not match.'}
    return render(request, 'bank_app/sign_up.html', context)

@login_required
def single_customer(request, pk):
    user = User.objects.get(pk=pk)
    accounts = Account.objects.filter(user=user)
    context = {
        'user': user,
        'accounts': accounts,
    }
    return render(request, 'bank_app/single_customer.html', context)

@login_required
def update_status(request):
    if request.method == "POST":
        pk = request.POST['pk']
        user = User.objects.get(pk=pk)
        userprofile = get_object_or_404(UserProfile, user=user)
        membership = request.POST['membership']
        if user:
            userprofile.membership = membership
            userprofile.save()
    return HttpResponseRedirect(request.META['HTTP_REFERER'])